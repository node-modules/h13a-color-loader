var sh = require("shelljs");
var path = require("path");

var transformer = require("../h13a-autoadaptive-style");

module.exports = function(source) {

  this.cacheable(true);

  var done = this.async();

  var configFile = "./colors.config.js";
  var configFilePath = path.resolve(configFile);

  // this file may frequently change and should not be cached.
  delete require.cache[require.resolve(configFilePath)];
  var config = require(configFilePath);

  this.addDependency(configFilePath);

  transformer.transform({
    config: config,
    source: source,
    done: done
  })

};
